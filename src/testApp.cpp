#include "testApp.h"

#include <iostream>

#include <string>
using std::string;

#define startKML "data/1OOOL.kml"
#define refreshKML "data/refresh.kml"

//--------------------------------------------------------------
void testApp::setup() {
ofSetWindowShape(830 , 750);

// enable depth->video image calibration
	kinect.setRegistration(true);
	//kinect.init(true);  //shows infrared image
	kinect.init();
//	kinect.setVerbose(true);
	kinect.open();

	colorImg.allocate(kinect.width, kinect.height);
	grayImage.allocate(kinect.width, kinect.height);
	grayDiff.allocate(kinect.width, kinect.height);
	grayPrevImage.allocate(kinect.width, kinect.height);
	grayThresh.allocate(kinect.width, kinect.height);
	grayThreshFar.allocate(kinect.width, kinect.height);

	nearThreshold = 230;
	farThreshold  = 70;
	bThreshWithOpenCV = true;

	motionThreshold = 80;

	ofSetFrameRate(60);

	// zero the tilt on startup
	angle = 0;
	kinect.setCameraTiltAngle(angle);

	// start from the front
	pointCloudRotationY = 180;

	drawPC = false;




	//HTTP server
	server = ofxHTTPServer::getServer(); // get the instance of the server
	server->setServerRoot("www");		 // folder with files to be served
	//server->setUploadDir("upload");		 // folder to save uploaded files
	server->setCallbackExtension("kml");	 // extension of urls that aren't files but will generate a post or get event
	//ofAddListener(server->getEvent,this,&testApp::getRequest);
	///ofAddListener(server->getEvent,this,&testApp::getRequest);
	ofAddListener(server->getEvent,this,&testApp::getRequest);
	server->start(10000);


//kml
hud=loadfile(startKML);//chargement du kml de depart
refresh=loadfile(refreshKML);//chargement du fichier de rafraichissement

///vol
vitesse=0;
vitesseKmh=0;
TimeDelay=0;
plancher=7;///forcement > 0
plafond=8000;
FacteurVitesse=30;///facteur entre la quantité de mouvement et la vitesse /sol
FacteurAltitude=1500000;
FacteurAngle= 5000;
cout <<"preferences ..."<< endl ;
loadprefs();







///le landreau
//coords=ofPoint(-1.302733561027899, 47.19841586620245, 111);///point de départ log,lat,alt
//atitude=ofPoint(-6.609532963909031,67.74893734597971,0);///heading,tilt,roll
cout <<"initialisation OK"<< endl ;
}

//--------------------------------------------------------------
void testApp::update() {
	ofBackground(100, 100, 100);



	kinect.update();
	if(kinect.isFrameNew())	// there is a new frame and we are connected
	{

		grayImage.setFromPixels(kinect.getDepthPixels(), kinect.width, kinect.height);

		//we do two thresholds - one for the far plane and one for the near plane
		//we then do a cvAnd to get the pixels which are a union of the two thresholds.
		if( bThreshWithOpenCV ){
			grayThreshFar = grayImage;
			grayThresh = grayImage;
			grayThresh.threshold(nearThreshold, true);
			grayThreshFar.threshold(farThreshold);
			cvAnd(grayThresh.getCvImage(), grayThreshFar.getCvImage(), grayImage.getCvImage(), NULL);
		}else{
			//or we do it ourselves - show people how they can work with the pixels
			unsigned char * pix = grayImage.getPixels();
			int numPixels = grayImage.getWidth() * grayImage.getHeight();
			for(int i = 0; i < numPixels; i++){
				if( pix[i] < nearThreshold && pix[i] > farThreshold ){
					pix[i] = 255;
				}else{
					pix[i] = 0;
				}
			}
		}



///mesure de la presence
PresenceFinder.findContours(grayImage, 100, (kinect.width*kinect.height), 3, false);
///calcul du centre de la zone de présence
int totalPresence=0;
for(int j= 0; j < PresenceFinder.blobs.size(); j++){
   totalPresence+= PresenceFinder.blobs[j].area;
    CentrePresence[0]+= PresenceFinder.blobs[j].area*((PresenceFinder.blobs[j].centroid.x*400/kinect.width)-200);
    CentrePresence[1]+=PresenceFinder.blobs[j].area*((PresenceFinder.blobs[j].centroid.y*300/kinect.height)-150);
}
if(totalPresence>0 && PresenceFinder.blobs.size()>0){
    CentrePresence=CentrePresence/(PresenceFinder.blobs.size()*totalPresence);
}


///image du mouvement
		grayDiff.absDiff(grayPrevImage, grayImage);
		grayDiff.threshold(motionThreshold);


///previous image in memory
grayPrevImage=grayImage;

///update the cv image
grayImage.flagImageChanged();

///éviter les frises de la detection
grayDiff.erode();
//grayDiff.blurGaussian(3);
grayDiff.erode();
//grayDiff.erode();

		/// find contours which are between the size of 20 pixels and 1/3 the w*h pixels.
    	// also, find holes is set to true so we will get interior contours as well....
    	motionDetector.findContours(grayDiff, 100, (kinect.width*kinect.height)/2, 20, true);



    	///extraction d'informations dynamiques d'apres les blobs detectes
    	nombremouvements=0;
    	ofPoint centroid;
    	int w=kinect.width;
    	int h=kinect.height;
        ///parcour de tous les blobs detectes
    	for(int i = 0; i < motionDetector.blobs.size(); i++){
    	    nombremouvements=nombremouvements+motionDetector.blobs[i].area;
            centroid[0] += motionDetector.blobs[i].area*(motionDetector.blobs[i].centroid.x);
            centroid[1] +=motionDetector.blobs[i].area*(motionDetector.blobs[i].centroid.y);
    	}



 if (motionDetector.blobs.size()>0 &&totalPresence>0){
     int numPixels=w*h;

     ///calcul du centroid moyen centré sur la zone de présence
     ///quantité de MVT/TOTALPRESENCE
    centroid[0] = ((centroid[0]/nombremouvements)-(w/2)-(CentrePresence[0]*kinect.width/400))*(nombremouvements*FacteurVitesse/totalPresence);
    centroid[1] =((centroid[1]/nombremouvements)-(h/2)-(CentrePresence[1]*kinect.height/300))*(nombremouvements*FacteurVitesse/totalPresence);

//quantite de MVT / SURFACE TOTALE
//    centroid[0] = ((centroid[0]/nombremouvements)-(w/2)-(CentrePresence[0]*kinect.width/400))*(nombremouvements*100/numPixels);
 //   centroid[1] =((centroid[1]/nombremouvements)-(h/2)-(CentrePresence[1]*kinect.height/300))*(nombremouvements*100/numPixels);


///ce calcul centre le mouvement sur le centr de l'image
    // centroid[0] = (( centroid[0]/nombremouvements)-(w/2))*(nombremouvements*100/numPixels);
     //centroid[1] =  ((centroid[1]/nombremouvements)-(h/2))*(nombremouvements*100/numPixels);
   }else{
     centroid[0]=0;
     centroid[1]=0;
   }


//centroid[0]=(centroid[0]*400/kinect.width);
//centroid[1]=(centroid[1]*300/kinect.height);
///calcul du centroid amorti
    Memcentroid[0]=Memcentroid[0]+((centroid[0]-Memcentroid[0])/14);
    Memcentroid[1]=Memcentroid[1]+((centroid[1]-Memcentroid[1])/14);


///vole...
fly();
	}


}

//--------------------------------------------------------------
void testApp::draw() {
	ofSetColor(255, 255, 255);


kinect.drawDepth(420, 320, 400, 300);
		//kinect.drawDepth(10, 10, 400, 300);
		kinect.draw(420, 10, 400, 300);

		grayImage.draw(10, 320, 400, 300);


		grayDiff.draw(10, 10, 400, 300);
		motionDetector.draw(10, 10, 400, 300);

		ofSetColor(250, 250, 250);




		PresenceFinder.draw(10, 10, 400, 300);

///direction du mouvement



ofSetColor(20, 210, 5);
///affichage des infos de vol

  ofNoFill();
  ///grand cercle

 int diametre= ofDist(CentrePresence.x,CentrePresence.y, ((Memcentroid[0])+(CentrePresence.x)),((Memcentroid[1])+(CentrePresence.y)) );
  ofCircle(CentrePresence.x+210,CentrePresence.y+160, diametre);

   ofFill();
//ofLine(CentrePresence.x+210,CentrePresence.y+160,((Memcentroid[0])+(CentrePresence.x+210)),((Memcentroid[1])+(CentrePresence.y+160)));
ofCircle(((Memcentroid[0])+(CentrePresence.x+210)),((Memcentroid[1])+(CentrePresence.y+160)), 10);



	ofSetColor(255, 255, 255);
	stringstream reportStream;
	reportStream << "Vitesse: " << ofToString(vitesseKmh, 3) <<" Km/h"<< ",  " << ofGetFrameRate() <<"fps:"<<  endl
				 << "press (1) et (4) pour changer la sensibilite du mouvement :" <<motionThreshold<< endl
				 << "press (j) et (k) pour changer FacteurVitesse :" <<FacteurVitesse<< endl
				 << "centroid " <<ofToString(Memcentroid[0],0)<<":"<<ofToString(Memcentroid[1],0)<< endl
				 << "Limite proche    :" << nearThreshold << " (press: + -)" <<endl
				 << "Limite lointaine :" << farThreshold << " (press: < >)"<< endl
				 //<< "press c to close the connection and o to open it again, connection is: " << kinect.isConnected() << endl
				 << "presse HAUT et BAS pour orienter la camera: " << angle << " degrees"<< endl
				 << "presse (s) pour sauver les preferences";
	ofDrawBitmapString(reportStream.str(),20,640);

}



//------------------------------------------------------FacteurVitesse--------
void testApp::exit() {
	kinect.setCameraTiltAngle(0); // zero the tilt on exit
	kinect.close();
}

//--------------------------------------------------------------
void testApp::keyPressed (int key) {
	switch (key) {
		case ' ':
			bThreshWithOpenCV = !bThreshWithOpenCV;
		break;
		case 'j':
			///FacteurVitesse-
            if(FacteurVitesse>1){
                FacteurVitesse-=1;
            }
		break;
		case 'k':
			///FacteurVitesse+
            if(FacteurVitesse<255){
                FacteurVitesse+=1;
            }
		break;
		case 'a':
			///attibution du point de départ stocke dans les preferences

            coords=ofPoint((XML.getValue("Depart:LookAt:longitude", 50.0)), XML.getValue("Depart:LookAt:latitude", 50.0), XML.getValue("Depart:LookAt:altitude", 50.0));
            atitude=ofPoint(XML.getValue("Depart:LookAt:heading", 0.0), XML.getValue("Depart:LookAt:tilt", 0.0), 0);


            cout <<"Lieu de depart : "<<coords<< endl ;
            cout <<"atitude : "<<atitude<< endl ;
		break;
		case 's':
		cout <<"sauvegarde"<< endl ;
            saveprefs();
        break;
		case '>':
		case '.':
			farThreshold ++;
			if (farThreshold > 255) farThreshold = 255;
			break;
        case '1':
			motionThreshold --;
			if (motionThreshold < 0) motionThreshold = 0;
			break;
        case '4':
			motionThreshold ++;
			if (motionThreshold > 255) motionThreshold = 255;
			break;
		case '<':
		case ',':
			farThreshold --;
			if (farThreshold < 0) farThreshold = 0;
			break;

		case '+':
		case '=':
			nearThreshold ++;
			if (nearThreshold > 255) nearThreshold = 255;
			break;
		case '-':
			nearThreshold --;
			if (nearThreshold < 0) nearThreshold = 0;
			break;
		case 'w':
			kinect.enableDepthNearValueWhite(!kinect.isDepthNearValueWhite());
			break;
		case 'o':
			kinect.setCameraTiltAngle(angle);	// go back to prev tilt
			kinect.open();
			break;
		case 'c':
			kinect.setCameraTiltAngle(0);		// zero the tilt
			kinect.close();
			break;

		case OF_KEY_UP:
			angle++;
			if(angle>30) angle=30;
			kinect.setCameraTiltAngle(angle);
			break;

		case OF_KEY_DOWN:
			angle--;
			if(angle<-30) angle=-30;
			kinect.setCameraTiltAngle(angle);
			break;
	}
}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y) {
	pointCloudRotationY = x;
}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button)
{}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button)
{}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button)
{}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h)
{}


//--------------------------------------------------------------
void testApp::getRequest(ofxHTTPServerResponse & response){


	if(response.url=="/lieues.kml"){
		response.response=hud;//renvoie le fichier kml de depart


	}else if(response.url=="/refresh.kml"){


	///atitude : heading,tilt,roll
	///coords : long,lat,alt
	///cout<<ofToString(coords.x,6)<<":"<<ofToString(coords[1],6)<<":"<<ofToString(coords[2],6)<<endl;

	vector <string> pagedata=ofSplitString(refresh, "*");//extrait le schema du kml de rafraichissement



	string sortie=pagedata[0]+ofToString(atitude[0],3)+pagedata[1];//screenOverlay.rotation
	sortie=sortie+ofToString(coords[0],6)+pagedata[2];//longitude
    sortie=sortie+ofToString(coords[1],6)+pagedata[3];//latitude
    sortie=sortie+ofToString(coords[2],3)+pagedata[4];//altitude
    sortie=sortie+ofToString(atitude[0],4)+pagedata[5];
    sortie=sortie+ofToString(atitude[1],4)+pagedata[6];
    sortie=sortie+ofToString(atitude[2],3)+pagedata[7];


	response.response=sortie;


	}else if(response.url=="/go.kml"){
	///récupération des paramètres
    cout <<"Saut instantané"<< endl ;
	///con_info->fields[key] = value;

	//cout <<response.requestFields["test"]<< endl ;



	coords=ofPoint(ofToFloat(response.requestFields["long"]), ofToFloat(response.requestFields["lat"]), ofToFloat(response.requestFields["alt"]));
	atitude=ofPoint(ofToFloat(response.requestFields["head"]), ofToFloat(response.requestFields["tilt"]), ofToFloat(response.requestFields["roll"]));





            response.response="ok";
	}

}

void testApp::postRequest(ofxHTTPServerResponse & response){
    /*
	if(response.url=="/postImage.of"){
		//postedImgName = response.requestFields["name"];
		///postedImgFile = response.uploadedFiles[0];
		//response.response = "<html> <head> oF http server </head> <body> image " + response.uploadedFiles[0] + " received correctly <body> </html>";
		response.response = "<html> <head> oF http server </head> <body> coucou les zamis <body> </html>";
	}
	*/
}

string testApp::loadfile(char* filename){
    string texte;
 FILE *fp; // fopen returns a file handle of type FILE *
   fp = fopen(filename, "r"); // in this case we open the file for reading
   int c;
   if(!fp){
     fprintf(stderr, "File - %s - could not be opened.\n", filename);
return "";
   }else{
   c = getc(fp) ;
   while (c!= EOF)
   {
   		//putchar(c);
   		texte=texte+char(c);
		c = getc(fp);

   }
   fclose(fp);
   fprintf(stderr, "succed to read File - %s - \n", filename);
   return texte;
   }

}

void testApp::fly(){
/// affecte le vol avec le mouvement

//angle affecté au vecteur vitesse
float angleVitesse=-90-ofRadToDeg((atan2(Memcentroid[0],Memcentroid[1])));

//mise à jour de la vitesse linéaire
vitesse =  vitesse+((nombremouvements-vitesse)/7);

///mise a jour du cap (heading=atitude[0])
float angleCorrige=atitude[0]+angleVitesse;


///coords[2] = altitude

///le nombre 5000000000... influe sur le rapport entre le mouvement à l'écran et le mouvement dans l'espace
float deplacement=(abs(Memcentroid[1])*(coords[2]/500000000));///deplacement en deg/affichage
float deplacementSol = deplacement*111.111;///en km



///calcul de l'intervalle de temps passé depuis la dernière mise à jour
 TimeDelay=ofGetElapsedTimeMillis()-TimeDelay;


///mise a jour de la vitesseKM
float newvitesse=deplacementSol*3600000/TimeDelay;
//vitesseKmh=vitesseKmh+((newvitesse-vitesseKmh)/17);
vitesseKmh=newvitesse;
TimeDelay=ofGetElapsedTimeMillis();

///mise à jour de la longitude
coords[0]-=deplacement * cos(ofDegToRad(angleCorrige)) ;

if (coords[0]>180){
    coords[0]=180;
  }
  if (coords[0]<-180){
    coords[0]=-180;
  }

  ///mise à jour de la latitude (coords[1])
coords[1]+=deplacement * sin(ofDegToRad(angleCorrige)) ;
  if (coords[1]>90){
    coords[1]=90;
  }
  if (coords[1]<-90){
    coords[1]=-90;
  }


  ///mise à jour de l'altitude (coords[2])
      coords[2]+=(pow(vitesse,2)/FacteurAltitude)-(0.01*coords[2]/plancher);

      /// FacteurAltitude

    if(coords[2]<plancher){
    coords[2]=plancher;
    }


  if(coords[2]>plafond){
    coords[2]=plafond;
  }


  ///atitude : heading,tilt,roll

  //heading=cap
   atitude[0]+=((Memcentroid[0])/(coords[2]*1800/plafond));///tourne plus à basse altitude
  if(atitude[0]>360){
    atitude[0] -=360;
  }
  if(atitude[0]<0){
    atitude[0] +=360;
  }


  ///tilt
  atitude[1]=(75)-(coords[2]*75/(FacteurAngle));

 if(atitude[1]>90){atitude[1]=90;}
 if(atitude[1]<0){atitude[1]=0;}


  ///roll

 atitude[2] =-(Memcentroid[0])/10.;

}

void testApp::loadprefs(){



    XML.loadFile("preferences.xml");

    nearThreshold = XML.getValue("settings:nearThreshold", 230);
    farThreshold = XML.getValue("settings:farThreshold", 70);
    motionThreshold = XML.getValue("settings:motionThreshold", 80);
    angle = XML.getValue("settings:angle", 0);
    plafond = XML.getValue("settings:plafond", 8000);
    FacteurVitesse = XML.getValue("settings:FacteurVitesse", 30);
    FacteurAltitude = XML.getValue("settings:FacteurAltitude", 1500000);
    FacteurAngle = XML.getValue("settings:FacteurAngle", 5000);

///lieu de depart

    coords=ofPoint((XML.getValue("Depart:LookAt:longitude", 50.0)), XML.getValue("Depart:LookAt:latitude", 50.0), XML.getValue("Depart:LookAt:altitude", 50.0));
	atitude=ofPoint(XML.getValue("Depart:LookAt:heading", 0.0), XML.getValue("Depart:LookAt:tilt", 0.0), 0);


    cout <<"Lieu de depart : "<<coords<< endl ;
    cout <<"atitude : "<<atitude<< endl ;


}

void testApp::saveprefs(){

XML.setValue("settings:nearThreshold",nearThreshold);
XML.setValue("settings:farThreshold",farThreshold);
XML.setValue("settings:motionThreshold",motionThreshold);
XML.setValue("settings:FacteurVitesse",FacteurVitesse);
XML.setValue("settings:angle",angle);


XML.saveFile("preferences.xml");
cout <<"sauvegarde OK"<< endl ;

}
